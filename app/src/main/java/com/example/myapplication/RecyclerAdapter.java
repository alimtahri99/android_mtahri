import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;

public class RecyclerAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

     holder.itemPays.setText(Country.countries[i].getName());
     holder.itemVille.setText(Country.countries[i].getCapital());
     String uri = Country.countries[i].getImgUri();
     Context c1 = holder.itemFlag.getContext();
     holder.itemPays.setText(Country.countries[i].getName());
      holder.itemFlag.setImageDrawable(c1.getResources().getDrawable(c1.getResources().getIdentifier(uri, null, c1.getPackageName())))


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private final TextView name;
        private final TextView capital;

        public ViewHolder(View view) {
            super(view);
            image=view.findViewById(R.id.image);
            name = (TextView) view.findViewById(R.id.text1);
            capital = (TextView) view.findViewById(R.id.text2);
        }

        public String getName() {
            return name;
        }

        public void setName(String capital) {
            this.name = name;
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used
     * by RecyclerView.
     */
    public RecyclerView(String[] dataSet) {
        localDataSet = dataSet;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.getName().setName(localDataSet[position]);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.length;
    }
}